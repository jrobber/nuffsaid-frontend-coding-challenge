import Api from './api';

describe('api', () => {
  let model;
  beforeEach(() => {
    model = new Api(2, 5);
  });

  it('model is not null', () => {
    expect(model).not.toBeNull();
  });
  it('generates a message while started', async () => {
    model.start();

    await wait(10);

    expect(model.messages.length).toBeTruthy();
  });
  it('stops messages on stop', async () => {
    model.start();
    await wait(10);
    const len = model.messages.length;
    model.stop();
    await wait(10);

    expect(model.messages.length).toEqual(len);
  });
  it('parses error, warning, and info messages', () => {
    model.messages = [
      {
        message: 'error',
        priority: 1,
      },
      {
        message: 'warning',
        priority: 2,
      },
      {
        message: 'info',
        priority: 3,
      },
    ];
    expect(model.errorMessages.length).toEqual(1);
    expect(model.errorMessages[0].message).toEqual('error');
    expect(model.errorMessages[0].priority).toEqual(1);
    expect(model.warningMessages.length).toEqual(1);
    expect(model.warningMessages[0].message).toEqual('warning');
    expect(model.warningMessages[0].priority).toEqual(2);
    expect(model.infoMessages.length).toEqual(1);
    expect(model.infoMessages[0].message).toEqual('info');
    expect(model.infoMessages[0].priority).toEqual(3);
  });
  it('clears one message', () => {
    model.messages = [
      {
        message: 'error',
        priority: 1,
      },
      {
        message: 'warning',
        priority: 2,
      },
      {
        message: 'info',
        priority: 3,
      },
    ];
    model.clearMessage('warning')();
    expect(model.messages.length).toEqual(2);
  });
  it('clears all messages', () => {
    model.messages = [
      {
        message: 'error',
        priority: 1,
      },
      {
        message: 'warning',
        priority: 2,
      },
      {
        message: 'info',
        priority: 3,
      },
    ];

    model.clearAllMessages();

    expect(model.messages.length).toEqual(0);
  });
});

async function wait(time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}
