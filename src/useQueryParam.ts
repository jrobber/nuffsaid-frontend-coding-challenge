import React from 'react';
import { useState } from 'react';

const getQuery = () => {
  if (typeof window !== 'undefined') {
    return new URLSearchParams(window.location.search);
  }
  return new URLSearchParams();
};

const useQueryParams = () => {
  return getQuery();
};

export default useQueryParams;
