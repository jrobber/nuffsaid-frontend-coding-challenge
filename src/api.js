import Chance from 'chance';
import lodash from 'lodash';
import { observable, makeObservable, computed, makeAutoObservable } from 'mobx';

/*
  I brought in mobx and converted this to an observable, because push streams like this do not play nicely with react functional components and hooks.
  This is the better tool for the job. And in general, a more versatile tool than react-hooks.
*/

class MessageGenerator {
  messages = [];
  constructor(timerLow, timerHigh, forcedPriority) {
    this.stopGeneration = false;
    this.chance = new Chance();
    this.timerLow = timerLow || 500;
    this.timerHigh = timerHigh || 3000;
    this.forcedPriority = forcedPriority;

    makeAutoObservable(this);
  }

  stop() {
    this.stopGeneration = true;
  }

  start() {
    this.stopGeneration = false;
    this.generate();
  }

  get isStarted() {
    return !this.stopGeneration;
  }

  get errorMessages() {
    return this.messages.filter((message) => message.priority === 1);
  }

  get warningMessages() {
    return this.messages.filter((message) => message.priority === 2);
  }
  get infoMessages() {
    return this.messages.filter((message) => message.priority === 3);
  }

  markMessageAsRead = (message) => {
    message.isNew = false;
  };

  clearMessage(message) {
    return () => {
      console.log('MessageGenerator -> clearMessage -> message', message);
      this.messages = this.messages.filter((m) => m.message != message);
    };
  }

  clearAllMessages = () => {
    this.messages = [];
  };

  /**
   * priority from 1 to 3, 1 = error, 2 = warning, 3 = info
   * */
  generate() {
    if (this.stopGeneration) {
      return;
    }
    console.log('MessageGenerator -> generate -> this.forcedPriority', this.forcedPriority)
    const message = this.chance.string();
    const priority = +(this.forcedPriority || lodash.random(1, 3));
    const nextInMS = lodash.random(this.timerLow, this.timerHigh);
    this.messages.unshift({
      message,
      priority,
      isNew: true,
      clearMessage: this.clearMessage(message),
    });
    setTimeout(() => {
      this.generate();
    }, nextInMS);
  }
}

export default MessageGenerator;
