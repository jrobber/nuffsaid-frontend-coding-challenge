import React, { FunctionComponent, useState, useCallback, useEffect, useRef } from 'react';
import Button from '@material-ui/core/Button';
import Api from '../api';
import { observer } from 'mobx-react-lite';
import { Card, MuiThemeProvider, CardContent, Typography, Snackbar } from '@material-ui/core';
import { string } from 'prop-types';
import useQueryParams from '../useQueryParam';

const flexStyles = {
  display: 'flex',
  alignItems: 'stretch',
  justifyContent: 'center',
};

const MessageList: FunctionComponent = observer((props) => {
  const { api, renderButton } = useMessagesApi();
  const [open, setOpen] = useState<string>('');

  useEffect(() => {
    if (api.errorMessages.length && api.errorMessages[0].isNew) {
      api.markMessageAsRead(api.errorMessages[0]);
      setOpen(api.errorMessages[0].message);
    }
  }, [api.errorMessages.length && api.errorMessages[0].isNew]);

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: 1,
        justifyContent: 'center',
      }}
    >
      <div
        style={{
          height: 40,
          borderStyle: 'solid',
          borderWidth: 0,
          borderBottomWidth: 1,
          borderColor: '#bababa',
          width: '5000px',
          justifySelf: 'center',
        }}
      >
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={!!open}
          onClose={() => setOpen('')}
          key={'topCenter'}
          autoHideDuration={2000}
        >
          <Card
            style={{
              backgroundColor: colors[1],
              padding: 12,
              marginBottom: 8,
              width: 350,
              ...flexStyles,
              justifyContent: 'start',
              alignItems: 'center',
            }}
          >
            <Button size="small" style={{ color: 'black' }} onClick={() => setOpen('')}>
              X
            </Button>
            {open}
          </Card>
        </Snackbar>
      </div>
      <div style={{ ...flexStyles, padding: 16 }}>
        <div>{renderButton()}</div>
        <Button style={{ backgroundColor: '#38DEB7', marginLeft: 4 }} onClick={api.clearAllMessages} id="clearAll">
          CLEAR
        </Button>
      </div>
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: 'repeat(3, minmax(350px, 1fr))',
          justifyContent: 'center',
          gap: 8,
        }}
      >
        <NotificationList title="Error Type 1" messages={api.errorMessages} />
        <NotificationList title="Warning Type 2" messages={api.warningMessages} />
        <NotificationList title="Info Type 3" messages={api.infoMessages} />
      </div>
    </div>
  );
});

export default MessageList;

interface IColor {
  [key: number]: string;
}

const colors = {
  '1': '#F56236',
  '2': '#FCE788',
  '3': '#88FCA3',
} as IColor;

const NotificationList: FunctionComponent<{
  title: string;
  messages: Array<{ message: string; priority: number; clearMessage: () => void }>;
}> = ({ title, messages }) => {
  return (
    <div
      style={{
        flex: 1,
      }}
      id={'nfl-' + title.split(' ')[0]}
    >
      <Typography
        style={{
          fontSize: 22,
        }}
      >
        {title}
      </Typography>
      <Typography
        style={{
          fontSize: 14,
          color: '#555555',
        }}
        className="count"
      >
        Count {messages.length}
      </Typography>
      {messages.map((message) => {
        return (
          <Card
            style={{
              backgroundColor: colors[message.priority],
              padding: 12,
              marginBottom: 8,
              ...flexStyles,
            }}
            className="notification-card"
            key={message.message}
          >
            <div style={{ flex: 1 }}>
              <Typography>{message.message}</Typography>
            </div>
            <Button onClick={message.clearMessage} style={{ padding: 4 }} className="clear-itm-btn">
              clear
            </Button>
          </Card>
        );
      })}
    </div>
  );
};

function useMessagesApi() {
  const query = useQueryParams();
  const [api, setApi] = useState(new Api(query.get('timerLow'), query.get('timerHigh'), query.get('forcedPriority')));

  useEffect(() => {
    api.start();
  }, []);

  const renderButton = () => {
    const isApiStarted = api.isStarted;
    return (
      <Button
        variant="contained"
        style={{ backgroundColor: '#38DEB7' }}
        onClick={() => {
          if (isApiStarted) {
            api.stop();
          } else {
            api.start();
          }
        }}
      >
        {isApiStarted ? 'STOP' : 'START'}
      </Button>
    );
  };

  return {
    api,
    renderButton,
  };
}

// class MessageList extends Component {
//   constructor(props, ...args: any[]) {
//     super(props, ...args);
//     this.state = {
//       messages: [],
//     };
//   }

//   api = new Api({
//     messageCallback: (message) => {
//       this.messageCallback(message);
//     },
//   });

//   componentDidMount() {
//     this.api.current.start();
//   }

//   messageCallback(message) {
//     const { messages } = this.state;
//     this.setState(
//       {
//         messages: [...messages.slice(), message],
//       },
//       () => {
//         // Included to support initial direction. Please remove upon completion
//         console.log(messages);
//       },
//     );
//   }

//   renderButton() {
//     const isApiStarted = this.api.current.isStarted();
//     return (
//       <Button
//         variant="contained"
//         onClick={() => {
//           if (isApiStarted) {
//             this.api.current.stop();
//           } else {
//             this.api.current.start();
//           }
//           this.forceUpdate();
//         }}
//       >
//         {isApiStarted ? 'Stop Messages' : 'Start Messages'}
//       </Button>
//     );
//   }

//   render() {
//     return (
//       <div>
//         {this.renderButton()}
//         Code here!
//       </div>
//     );
//   }
// }

// export default MessageList;
