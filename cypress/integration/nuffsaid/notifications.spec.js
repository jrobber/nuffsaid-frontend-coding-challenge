/// <reference types="cypress" />
// remove no check once Cypress.sinon is typed
// https://github.com/cypress-io/cypress/issues/6720

context('notifications', () => {
  it('keeps counts in sync - errors', () => {
    // https://on.cypress.io/spy
    cy.visit('http://localhost:3000', {
      qs: { timerLow: 2, timerHigh: 10, forcedPriority: 1  },
    });

    cy.contains('STOP');

    cy.wait(20);

    cy.contains('STOP').click();

    cy.get('#nfl-Error .notification-card')
      .its('length')
      .then((listCount) => {
        cy.get('#nfl-Error .count').should('have.text', 'Count ' + listCount);
      });
  });
  it('keeps counts in sync - warnings', () => {
    // https://on.cypress.io/spy
    cy.visit('http://localhost:3000', {
      qs: { timerLow: 2, timerHigh: 10, forcedPriority: 2  },
    });

    cy.contains('STOP');

    cy.wait(20);

    cy.contains('STOP').click();


    cy.get('#nfl-Warning .notification-card')
      .its('length')
      .then((listCountWarning) => {
        cy.get('#nfl-Warning .count').should('have.text', 'Count ' + listCountWarning);
      });
  });
  it('keeps counts in sync - info', () => {
    // https://on.cypress.io/spy
    cy.visit('http://localhost:3000', {
      qs: { timerLow: 2, timerHigh: 10, forcedPriority: 3  },
    });

    cy.contains('STOP');

    cy.wait(20);

    cy.contains('STOP').click();


    cy.get('#nfl-Info .notification-card')
      .its('length')
      .then((listCountInfo) => {
        cy.get('#nfl-Info .count').should('have.text', 'Count ' + listCountInfo);
      });
  });

  it('clears an item', () => {
    // https://on.cypress.io/spy
    cy.visit('http://localhost:3000', {
      qs: { timerLow: 2, timerHigh: 3, forcedPriority: 1 },
    });
    cy.contains('STOP');

    cy.wait(100);

    cy.contains('STOP').click();

    const errorListComponent = cy.get('#nfl-Error');

    // errorListComponent.find('.clear-itm-btn').its('length').should('be.greaterThan', 2);

    errorListComponent
      .find('.clear-itm-btn')
      .its('length')
      .then((len) => {
        cy.get('#nfl-Error .clear-itm-btn').first().click();
        cy.get('#nfl-Error .clear-itm-btn').its('length').should('be.lessThan', len);
      });
  });

  // it('clears all items', () => {
  //   // https://on.cypress.io/spy
  //   cy.visit('http://localhost:3000?timerLow=2&timerHigh=3');

  //   cy.wait(10);
  //   cy.contains('STOP').click();
  //   const targets = errorListComponent.find('.notification-card');
  //   expect(targets.length).to.be.greaterThan(0);

  //   const errorListComponent = cy.get('#clearAll').click();

  //   const targets2 = errorListComponent.find('.notification-card');
  //   expect(targets2.length).to.be.equal(0);
  // });

  // it('places new messages on top', () => {
  //   // https://on.cypress.io/spy
  //   cy.visit('http://localhost:3000?timerLow=2&timerHigh=2?forcedPriority=1');

  //   cy.wait(10);
  //   cy.contains('STOP').click();
  //   const targets = errorListComponent.find('.notification-card').text;

  //   cy.contains('START').click();
  //   cy.wait(10);
  //   cy.contains('STOP').click();
  //   const targets2 = errorListComponent.find('.notification-card').text;

  //   expect(targets2.indexOf(targets[0])).to.be.greaterThan(0);
  // });
});
